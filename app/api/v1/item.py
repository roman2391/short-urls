import string
import random

from datetime import datetime, timedelta

from sqlalchemy.orm import Session
from fastapi.templating import Jinja2Templates
from fastapi.responses import RedirectResponse, HTMLResponse
from fastapi import Depends, HTTPException, status, APIRouter, Response, Request

from app.models import models
from app.db.base import get_db
from app.schemas import schemas


router = APIRouter()
templates = Jinja2Templates(directory="app/templates")


@router.get("/main/", response_class=HTMLResponse)
def main_page(request: Request):
    return templates.TemplateResponse("main.html", {"request": request})


@router.get('/item/{short_url}')
def get_item(db: Session = Depends(get_db), short_url: str = None):
    item = db.query(models.Item).filter(models.Item.short_url.contains(short_url)).first()
    if item is not None:
        if item.created_at + timedelta(days=item.ttl_days) < datetime.now():
            return {"status": f"url - {item.short_url} has expired"}
        return RedirectResponse(item.origin_url)
    return RedirectResponse("/")


@router.post("/item/", response_model=schemas.ResponseItem)
def create_item(item: schemas.CreateItem, db: Session = Depends(get_db)):
    chars = string.ascii_lowercase + string.ascii_uppercase
    unique_string = ''.join(random.sample(chars, 6))
    db_item = models.Item(short_url=unique_string, origin_url=item.origin_url, ttl_days=item.ttl_days)
    db.add(db_item)
    db.commit()
    db.refresh(db_item)
    return db_item


@router.delete('/item/{short_url}')
def get_item(db: Session = Depends(get_db), short_url: str = None):
    item_query = db.query(models.Item).filter(
        models.Item.short_url == short_url
    )
    if not item_query.count():
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f'No such url {short_url} found'
        )

    item_query.delete(synchronize_session=False)
    db.commit()

    return Response(status_code=status.HTTP_204_NO_CONTENT)
