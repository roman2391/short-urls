import uvicorn

from datetime import datetime, timedelta

from sqlalchemy.orm import Session
from fastapi import FastAPI, Request, Depends
from fastapi.templating import Jinja2Templates
from fastapi.responses import RedirectResponse
from fastapi.middleware.cors import CORSMiddleware

from app.api.v1 import item
from app.models import models
from app.db.base import engine, Base, get_db


app = FastAPI()

Base.metadata.create_all(bind=engine)

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

templates = Jinja2Templates(directory="app/templates")

app.include_router(item.router, tags=['Items'], prefix='/api')

@app.get("/")
def root():
    return RedirectResponse("/api/main")


@app.get("/{short_url}")
def redirect_to_origin(request: Request, db: Session = Depends(get_db), short_url: str = None):
    item = db.query(models.Item).filter(models.Item.short_url.contains(short_url)).first()
    if item is not None:
        if item.short_url == "main":
            return templates.TemplateResponse("main.html", {"request": request})
        if item.created_at + timedelta(days=item.ttl_days) < datetime.now():
            return {"status": f"url - {item.short_url} has expired"}
        return RedirectResponse(item.origin_url)
    return RedirectResponse("/api/main")


uvicorn.run(app, host="127.0.0.1", port=8000)
