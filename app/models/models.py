from app.db.base import Base
from sqlalchemy import TIMESTAMP, Column, String, Integer, text


class Item(Base):
    __tablename__ = 'items'

    id = Column(Integer, primary_key=True, nullable=False)
    short_url = Column(String, unique=True,  nullable=False)
    origin_url = Column(String,  nullable=False)
    ttl_days = Column(Integer, nullable=False, default=90)
    created_at = Column(
        TIMESTAMP(timezone=False), nullable=False, server_default=text("now()")
    )
    updated_at = Column(
        TIMESTAMP(timezone=False), nullable=False, server_default=text("now()")
    )
