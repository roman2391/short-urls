from datetime import datetime
from pydantic import BaseModel


class CreateItem(BaseModel):
    origin_url: str
    ttl_days: int

    class Config:
        orm_mode = True


class ResponseItem(BaseModel):
    short_url: str
    origin_url: str
    ttl_days = int

    created_at: datetime
    updated_at: datetime

    class Config:
        orm_mode = True
